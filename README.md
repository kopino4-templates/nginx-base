# Nginx base reverse-proxy project

A boilerplate project for a reverse-proxy.

## Intro

So you just decided to add a NGINX reverse-proxy in front of your application.

The question is: What should you do next?

Should you create one from scratch? Find a silver bullet solution? Or is it
better to customize an already project?

I'm going to show you how to use this template to get a headstart in creating
a reverse-proxy with NGINX.

## Feature overview

*   [x] **SSL** for secure traffix between the client and server
*   [x] **HTTP to HTTPS** redirects to ensure secure communication
*   [x] **Redirect traffic** to your final destination servers

## Contents

*   [What is this?](#what-is-this)
*   [When should I use this?](#when-should-i-use-this)
*   [Getting started](#getting-started)
    *   [Requirements](#requirements)
    *   [Install](#install)
    *   [Configuration](#configuration)
    *   [Usage](#usage)
*   [Here is where it's your turn](#here-is-where-its-your-turn)
*   [About](#about)
    * [Used Technologies](#used-technologies)
    * [Testing](#testing)
    * [Logging](#logging)
*   [Contribute](#contribute)
*   [License](#license)
*   [Sources](#sources)
*   [Conclusion](#conclusion)

## What is this?

This project is an exhaustive README template that you can customize to your needs.
You can either add sections you like or remove sections you don't like. But you have
every time an example in front of you, from which you can derive from.

## Why should I use this?

There are many templates you can choose from. This one is for you to use with the most
important things set up. It is then on you to customize the template to your needs
and implement your Use cases.

## Getting Started

So how do you get this template to work for your project? It is easier than you think.

### Requirements

* Have a project ready in front of which you want to place your reverse proxy
* Basic knowledge of [NGINX][nginx-guide]

### Install

Use git to clone this repository into your computer.

```
git clone https://gitlab.com/kopino4-templates/nginx-base
```

### Configuration

To configure the application create a `.env` file following the `.env.example` structure

The main configuration file is `nginx.conf`. In this file all the virtual
server configuration files placed in the `conf.d` folder are automatically included.

There is a `conf.d/default.conf` configuration file. Here you can edit the SSL certificate
paths to match your newly generated, CA signed certificates.

If you want to specify a subdomain configuration use the `conf.d/subdomain_example.conf` as and example.
Dont't forget to point the SSL certificate paths to your subdomain certificate.

### Usage

Run in development mode

```bash
./scripts/dev_run.sh
```

Run in production mode

```bash
./scripts/prod_run.sh
```

Then inside the container

Generate SSL certificates

The server uses by default selfsigned certificates. If you want to get
CA signed certificates for your domain, use **Cerbot** (only in **production** build).

Run `./run_certbot.sh` and follow the steps. Don't forget to add all your subdomains.

## About

Let's take deep dive into the template

### Used technologies

We are using of course NGINX as a server to implement our reverse proxy in

To secure our traffic we are using the [Certbot][certbot] for automatic SSL
certificates generation

### Testing

No tests are implemented yet

### Logging

No logging is implemented yet

## Contribute

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)

## Sources

[Nginx guide][nginx-guide] - Learn basics of NGINX

[About Certbot][certbot] - A tool for generating SSL certificates

[//]: # "Source definitions"
[nginx-guide]: https://nginx.org/en/docs/beginners_guide.html "Nginx guide"
[certbot]: https://certbot.eff.org/pages/about "About Certbot"

## Conclusion

To summarize..

We have a NGINX template supporting secure communication through HTTPS and redirects
traffic to our desired server. In our future projects we can use this template to get
a great head start in creating a custom Reverse proxy with NGINX.

