##
## Obtain certificates and change configuration automatically
##
## Note: All the NGINX configuration changes are managed by Certbot
##

certbot --nginx

echo ""
echo "If Certbot has been run successfuly, it automatically changed the"
echo "SSL certificate paths in './conf.d/..'. If you want to permanently"
echo "keep those changes, edit './templates/..' folder and "
echo "restart the container "
