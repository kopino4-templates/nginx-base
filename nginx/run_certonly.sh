##
## Obtain certificate without changing NGINX configuration
##
## Note: You need to change the NGINX configuration manually
##

certbot --nginx certonly

echo ""
echo "If Certbot has been run successfuly, change the SSL"
echo "certificate paths in './templates/..' folder and "
echo "restart the container "
